<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h3>Nettopalkka</h3>
    <?php
    $brutto = filter_input(INPUT_POST,"brutto",FILTER_SANITIZE_NUMBER_FLOAT,FILTER_FLAG_ALLOW_FRACTION);
    $ennakko = filter_input(INPUT_POST,"ennakko",FILTER_SANITIZE_NUMBER_FLOAT,FILTER_FLAG_ALLOW_FRACTION);
    $tyoelake = filter_input(INPUT_POST,"tyoelake",FILTER_SANITIZE_NUMBER_FLOAT,FILTER_FLAG_ALLOW_FRACTION);
    $vakuutus = filter_input(INPUT_POST,"vakuutus",FILTER_SANITIZE_NUMBER_FLOAT,FILTER_FLAG_ALLOW_FRACTION);

    $nettopalkka = ($brutto - ($brutto * ($ennakko / 100)) - ($brutto * ($tyoelake / 100)) - ($brutto * ($vakuutus / 100)));

    printf("<p>Nettopalkka on %.2f</p>",$nettopalkka);
    ?>
    <a href="index.html">Laske uudestaan</a>
</body>
</html>